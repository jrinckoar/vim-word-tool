# Word Tools Vim Plugins
==================================================================
## Juan Felipe Restrepo. 
## jf.restrepo.rinckoar@bioingenieria.edu.ar.
==================================================================

### Word tools compilation of vim plugins

1. Thesaurus: search for synonyms and antonyms in thesaurus.

2.  Dictionary:  search for word's definition  in the "RAE" if language is  set to Spanish or in "dict" if  it is set to
Englis.

3. Gtranslator: translate a word/sentence from English/Spanish to Spanish/English.

-----------------------------------------------------------
