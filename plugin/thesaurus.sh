#!/bin/bash

#######################################
# Get user input                      #
#######################################

# Setup
BROWSER=$(which lynx)

DIVIDER="<hr />"
SUB_DIVIDER="<p>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</p>"
LINE_BREAK="<br />"
HTML2TEXT_CMD="html2text"
PAGER_CMD="less -r"
unset LESS
PAGE_SECTION="none"
SEARCH_RESULT=""
TEXT_VALID=0
SKIP_LINE=0

# Check Browser
if [ -z "$BROWSER" ]; then
   echo -e "\a$(basename $0) error: 'lynx' in not installed."
   exit 1
fi

for i in "$@"
do
case $i in
    *-h | *-help)
        HELP='thesaurus <text>' 
    ;;        
    *)
    ;;
esac
done

if [[ -n $HELP ]]; then
    echo $HELP
    exit
fi

SEARCH_TERM=$(echo $ | sed -e 's/^ *//' -e 's/ *$//')

# Get input words, replacing spaces with plus symbols
SEARCH_TERM=$(echo "$@" | sed -e 's/ /%20/g')
LOOKUP_URL="http://thesaurus.com/browse/${SEARCH_TERM}?s=t"

# Perform search                      
IFS_BAK="$IFS"
IFS=$'\n'

# Read and parse web page
# (NB: Due to the 'IFS' setting, it's awkward to use a
#  $BROWSER_CMD variable... so we write 'lynx' explicitly...)
for LINE in $(lynx -source "$LOOKUP_URL")
do

    SKIP_LINE=0

    # Determine current section of page...
    if [[ "$LINE" == *"<div id=\"synonyms"* ]]
    then
        SEARCH_RESULT+="${LINE_BREAK}${DIVIDER}${LINE_BREAK}Synonyms of '$@':${LINE_BREAK}"
        PAGE_SECTION="synonym"
    elif [[ "$LINE" == *"<div id=\"concept-thes"* ]]
    then
        SEARCH_RESULT+="${LINE_BREAK}${DIVIDER}${LINE_BREAK}'Concept' thesaurus for '$@':${LINE_BREAK}"
        PAGE_SECTION="concept"
    elif [[ "$LINE" == *"<div id=\"right-sidebar"* ]] # Not sure how else to identify end of useful content...
    then
        PAGE_SECTION="none"
    fi
    

    # Process current section
    case "$PAGE_SECTION" in
        "synonym" )

            if [[ "$LINE" == *"<div class=\"heading-row synonims-heading\">"* || "$LINE" == *"<ul"* ]]
            then
                TEXT_VALID=1
            elif [[ "$LINE" == *"<section class=\"container-info antonyms"* ]]
            then
                SEARCH_RESULT+="${LINE_BREAK}${SUB_DIVIDER}${LINE_BREAK}Related antonyms:${LINE_BREAK}"
            elif [[ "$LINE" == *"</div>"* ]]
            then
                TEXT_VALID=0
            fi

            if [[ "$LINE" == *"<span class=\"star "* ]]
            then
                SKIP_LINE=1
            fi

            ;;
        "concept" )

            if [[ "$LINE" == *"<ul"* ]]
            then
                TEXT_VALID=1
            elif [[ "$LINE" == *"<h3>"* && "$LINE" != *"phrases"* ]] # Too awkward to deal with phrase entries...
            then
                SEARCH_RESULT+="${LINE_BREAK}${SUB_DIVIDER}${LINE_BREAK}<p>$(echo "$LINE" | sed 's/<[^>]*>//g')</p>"
                TEXT_VALID=0
            elif [[ "$LINE" == *"</div>"* ]]
            then
                TEXT_VALID=0
            fi

            ;;
        * )

            TEXT_VALID=0

            ;;
    esac

    if [[ $TEXT_VALID == 1 && $SKIP_LINE == 0 ]]
    then
        SEARCH_RESULT+="$MARK$LINE"
    fi

done
# Display results
# (Converting random underscores to spaces, to improve readability...)
if [[ -z "$SEARCH_RESULT" ]];then
    echo "The word/expresion \"${SEARCH_TERM}\" has not been found on thesaurus.com!"
else
    SEARCH_RESULT+="${LINE_BREAK}${DIVIDER}"
    IFS="$IFS_BAK"
    echo "$SEARCH_RESULT" | $HTML2TEXT_CMD | sed -e 's/_/ /g' | $PAGER_CMD 
fi


