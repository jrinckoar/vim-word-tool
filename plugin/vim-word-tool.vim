" Word Tool Vim plugin 
" Author:       Juan Felipe Restrepo
" Version:      0.1

if exists("g:loaded_wordtool")
    finish
endif
let g:loaded_wordtool = 1

let s:save_cpo = &cpo
set cpo&vim

let s:path = expand("<sfile>:p:h")


function! s:GetVisual() range 
        let reg_save = getreg('"') 
        let regtype_save = getregtype('"') 
        let cb_save = &clipboard 
        set clipboard& 
        normal! ""gvy 
        let selection = getreg('"') 
        call setreg('"', reg_save, regtype_save) 
        let &clipboard = cb_save 
        return selection 
endfunction 

function! s:FindLanguage(lang) "{{{1
  " This replaces things like en_gb en-GB as expected by gtranslator,
  " only for languages that support variants in gtranslator.
  let l:language = substitute(substitute(a:lang,
  \  '\(\a\{2,3}\)\(_\a\a\)\?.*',
  \  '\=tolower(submatch(1)) . toupper(submatch(2))', ''),
  \  '_', '-', '')

  " All supported languages (with variants) from version gtranslator-1.8.
  let l:supportedLanguages =  {
  \  'ast'   : 1,
  \  'be'    : 1,
  \  'br'    : 1,
  \  'ca'    : 1,
  \  'cs'    : 1,
  \  'da'    : 1,
  \  'de'    : 1,
  \  'el'    : 1,
  \  'en'    : 1,
  \  'eo'    : 1,
  \  'es'    : 1,
  \  'fr'    : 1,
  \  'gl'    : 1,
  \  'is'    : 1,
  \  'it'    : 1,
  \  'km'    : 1,
  \  'lt'    : 1,
  \  'ml'    : 1,
  \  'nl'    : 1,
  \  'pl'    : 1,
  \  'pt'    : 1,
  \  'ro'    : 1,
  \  'ru'    : 1,
  \  'sk'    : 1,
  \  'sl'    : 1,
  \  'sv'    : 1,
  \  'tl'    : 1,
  \  'uk'    : 1,
  \  'zh'    : 1
  \}

  if has_key(l:supportedLanguages, l:language)
    return l:language
  endif
  " Removing the region (if any) and trying again.
  let l:language = substitute(l:language, '-.*', '', '')
  return has_key(l:supportedLanguages, l:language) ? l:language : ''
endfunction

function! s:GtranslatorLookUp(word,lang)
    silent keepalt belowright split gtranslator
    setlocal noswapfile nobuflisted nospell nowrap modifiable
    setlocal buftype=nofile bufhidden=hide
    1,$d
    echo "Requesting googleTranslator.com to look up the word \"" . a:word . "\"..."
    if a:lang == 'es'
        exec ":silent 0r !" . s:path . "/gtranslator.sh " ."-s=auto -t=en"." ". a:word 
    endif
    if a:lang == 'en'
         exec ":silent 0r !" . s:path . "/gtranslator.sh " ."-s=auto -t=es"." ". a:word 
    endif
    normal! Vgqgg
    exec 'resize ' . (line('$') - 1)
    setlocal nomodifiable filetype=gtranslator
    nnoremap <silent> <buffer> q :q<CR>
endfunction

function! s:DictionaryLookUp(word,lang)
    silent keepalt belowright split dictionary
    setlocal noswapfile nobuflisted nospell nowrap modifiable
    setlocal buftype=nofile bufhidden=hide
    1,$d
    echo "Requesting dictionary to look up the word \"" . a:word . "\"..."
    if a:lang == 'es'
        exec ":silent 0r !" . s:path . "/dictionary.sh " ."-l=es"." ". a:word 
    endif
    if a:lang == 'en'
        exec ":silent 0r !" . s:path . "/dictionary.sh " ."-l=en"." ". a:word 
    endif
    normal! Vgqgg
    exec 'resize ' . (line('$') - 1)
    setlocal nomodifiable filetype=dictionary
    nnoremap <silent> <buffer> q :q<CR>
endfunction

function! s:ThesaurusLookUp(word)
    silent keepalt belowright split thesaurus
    setlocal noswapfile nobuflisted nospell nowrap modifiable
    setlocal buftype=nofile bufhidden=hide
    1,$d
    echo "Requesting thesaurus.com to look up the word \"" . a:word . "\"..."
    exec ":silent 0r !" . s:path . "/thesaurus.sh " . a:word
    normal! Vgqgg
    exec 'resize ' . (line('$') - 1)
    setlocal nomodifiable filetype=thesaurus
    nnoremap <silent> <buffer> q :q<CR>
endfunction


if !exists('g:wordtool_map_keys')
    let g:wordtool_map_keys = 1
endif

if g:wordtool_map_keys
    noremap <LocalLeader>gd :DictionaryCurrentWord<CR>
    vnoremap <LocalLeader>gd :<C-u>DictionaryVisual<CR>
    noremap <LocalLeader>gs :ThesaurusCurrentWord<CR>
    vnoremap <LocalLeader>gs :<C-u>ThesaurusVisual<CR>
    noremap <LocalLeader>gt :GtranslatorCurrentWord<CR>
    vnoremap <LocalLeader>gt :<C-u>GtranslatorVisual<CR>
endif

command! DictionaryCurrentWord :call <SID>DictionaryLookUp(expand('<cword>'),<SID>FindLanguage(&spelllang))
command! DictionaryVisual :call <SID>DictionaryLookUp(<SID>GetVisual(),<SID>FindLanguage(&spelllang))
command! GtranslatorCurrentWord :call <SID>GtranslatorLookUp(expand('<cword>'),<SID>FindLanguage(&spelllang))
command! GtranslatorVisual :call <SID>GtranslatorLookUp(<SID>GetVisual(),<SID>FindLanguage(&spelllang))
command! ThesaurusCurrentWord :call <SID>ThesaurusLookUp(expand('<cword>'))
command! ThesaurusVisual :call <SID>ThesaurusLookUp(<SID>GetVisual())
let &cpo = s:save_cpo
