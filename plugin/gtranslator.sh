#!/bin/bash
# access translate.google.com from terminal

for i in "$@"
do
case $i in
    -t=*)
        TARGET=$(echo $i | sed 's/[-a-zA-Z0-9]*=//')
    ;;
    -s=*)
        SOURCE=$(echo $i | sed 's/[-a-zA-Z0-9]*=//')
    ;;
    *-h | *-help)
        HELP='gtranslate -s=source -t=target <text> 
        if target missing, use DEFAULT_TARGET_LANG=es.
        if source missing, use auto.'
    ;;        
    *)
    ;;
esac
done

if [[ -n $HELP ]]; then
    echo $HELP
    exit
fi

TEXT=$(echo $@ | sed -e  "s/-t=$TARGET//g")
TEXT=$(echo $TEXT | sed -e  "s/-s=$SOURCE//g")
TEXT=$(echo $TEXT | sed -e 's/^ *//' -e 's/ *$//')



# Default values
DEFAULT_TARGET_LANG=es

if [[ -z $SOURCE ]]; then
    SOURCE='auto'
fi
if [[ -z $TARGET ]]; then
    TARGET=$DEFAULT_TARGET_LANG
fi

#SEARCH_TERM=$(echo "$TEXT" | sed -e 's/ /%20/g')
SEARCH_TERM=$TEXT

result=$(curl -s -i --user-agent "" -d "sl=$SOURCE" -d "tl=$TARGET" --data-urlencode "text=$SEARCH_TERM" http://translate.google.com)
encoding=$(awk '/Content-Type: .* charset=/ {sub(/^.*charset=["'\'']?/,""); sub(/[ "'\''].*$/,""); print}' <<<"$result")
OUT=$(iconv -f $encoding <<<"$result" | awk 'BEGIN {RS="<div"};/<span[^>]*id=["'\'']?result_box["'\'']?/{sub(/^.*id=["'\'']?result_box["'\'']?(>| [^>]*>)([\n\t]*<[^>]*>)*/,"");sub(/<.*$/,"");print}' | html2text -utf8)
 
TEMP=($SEARCH_TERM)
if [[ "${#TEMP[@]}" -gt 10 ]];then
    echo "$SOURCE|$TARGET"
    echo "RESULT: ${OUT}"
else
    echo "$SOURCE|$TARGET"
    echo "ENTRY: ${SEARCH_TERM}"
    echo "RESULT: ${OUT}"
fi
exit
