
#!/bin/bash


# Setup
RAESERVER="lema.rae.es"
BROWSER=$(which lynx)
DEFAULT_DIC_LANG='en'

# Check Browser
if [ -z "$BROWSER" ]; then
   echo -e "\a$(basename $0) error: 'lynx' in not installed."
   exit 1
fi

for i in "$@"
do
case $i in
    -l=*)
        DIC_LANG=$(echo $i | sed 's/[-a-zA-Z0-9]*=//')
    ;;
    *-h | *-help)
        HELP='dictionary -l=language <text> 
        if language is missing, use DEFAULT_TARGET_DIC_LANG=en.'
    ;;        
    *)
    ;;
esac
done

if [[ -n $HELP ]]; then
    echo $HELP
    exit
fi

if [[ -z $DIC_LANG ]]; then
    DIC_LANG=$DEFAULT_DIC_LANG
fi

TEXT=$(echo $@ | sed -e  "s/-l=$DIC_LANG//g")
TEXT=$(echo $TEXT | sed -e 's/^ *//' -e 's/ *$//')

if [[ "${DIC_LANG}" == 'es' ]]; then
    QUERY=$(echo $TEXT | iconv -f UTF-8 -t LATIN1)
    $BROWSER -dump -width 75 "$RAESERVER/drae/srv/search?val=$QUERY" > /tmp/rae.tmp 2> /dev/null
    if [ "$?" -ne "0" ]; then
        echo -e "\a$(basename $0) error: can not connect to $raeserver"
        exit 1      
    fi
    sed '/Referencias/,$d' /tmp/rae.tmp | sed -e :a -e '/^\n*$/{$d;N;ba' -e '}'
    echo ""
fi

if [[ "${DIC_LANG}" == 'en' ]]; then
    dict -d gcide $TEXT
    echo ""
fi
exit 
